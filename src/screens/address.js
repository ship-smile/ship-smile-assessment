import addressActions from '../reduxs/address/actions'
import { connect } from 'react-redux';
import React, { useEffect } from 'react';
import { useForm } from "react-hook-form";

const Address = (props) => {
    const { register, handleSubmit } = useForm();
    useEffect(() => {
        // Update the document title using the browser API
        props.getAddress()
    }, []);
    const onSubmit = ({ keyword }) => {
        console.log(keyword)
        props.getAddress(keyword)
    };


    return (
        <div className="px-4 sm:px-6 lg:px-8">
            <div className="sm:flex sm:items-center justify-end">
                <form className="mt-5 sm:flex sm:items-center" onSubmit={handleSubmit(onSubmit)}>
                    <div className="w-full sm:max-w-xs">
                        <input
                            type="text"
                            className="block w-80 p-2 rounded-md border-red-500 shadow-sm focus:border-red-500 focus:ring-red-500"
                            placeholder="type address"
                            {...register("keyword", { required: true })}
                        />
                    </div>
                    <button
                        type="submit"
                        className="mt-3 inline-flex w-full items-center justify-center rounded-md border border-transparent bg-red-600 px-4 py-2 font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-offset-2 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                    >
                        Search
                    </button>
                </form>
            </div>
            <div className="mt-8 flex flex-col">
                <div className="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div className="inline-block min-w-full py-2 align-middle">
                        <div className="overflow-hidden shadow-sm ring-1 ring-black ring-opacity-5">
                            <table className="min-w-full divide-y divide-gray-300">
                                <thead className="bg-gray-50">
                                    <tr>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            TA_ID
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            TAMBON_T
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            TAMBON_E
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            AM_ID
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            AMPHOE_T
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            AMPHOE_E
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            CH_ID
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            CHANGWAT_T
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            CHANGWAT_E
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            LAT
                                        </th>
                                        <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                            LONG
                                        </th>


                                    </tr>
                                </thead>
                                <tbody className="divide-y divide-gray-200 bg-white">
                                    {props.data.length > 0 ? props.data.map(({
                                        [`District.Province.code`]: provinceCode,
                                        [`District.Province.id`]: provinceId,
                                        [`District.Province.nameInEnglish`]: provinceNameInEnglish,
                                        [`District.Province.nameInThai`]: provinceNameInThai,
                                        [`District.id`]: districtId,
                                        [`District.nameInEnglish`]: districtNameInEnglish,
                                        [`District.nameInThai`]: districtNameInThai,
                                        [`id`]: subdistrictId,
                                        [`latitude`]: latitude,
                                        [`longitude`]: longitude,
                                        [`nameInEnglish`]: subdistrictNameInEnglish,
                                        [`nameInThai`]: subdistrictNameInThai,
                                    }) => (
                                        <tr key={latitude + longitude}>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{subdistrictId}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{subdistrictNameInThai}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{subdistrictNameInEnglish}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{districtId}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{districtNameInThai}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{districtNameInEnglish}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{provinceId}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{provinceNameInThai}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{provinceNameInEnglish}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{latitude}</td>
                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{longitude}</td>
                                        </tr>
                                    )) : ""}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const { getAddress } = addressActions
const mapState = ({ addressReducers }) => {
    return {
        ...addressReducers
    };
};
export default connect(mapState, { getAddress })(Address);