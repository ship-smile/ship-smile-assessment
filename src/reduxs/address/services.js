import SuperFetch from '../../helpers/superFetch';
import { serialize } from "../../utils/serialize";

class services {
    get = async (keyword) => {
        return await SuperFetch.get(`address?${serialize({
            offset: 0,
            limit: 100,
            keyword
        })}`).then((response) => {
            return response;
        });
    };
}

export default new services();
