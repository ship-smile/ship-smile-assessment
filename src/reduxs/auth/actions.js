
const actions = {

    SIGN_IN_REQUEST: 'SIGN_IN_REQUEST',
    SIGN_IN_SUCCESS: 'SIGN_IN_SUCCESS',
    SIGN_IN_ERROR: 'SIGN_IN_ERROR',

    signin: (userCredentials) => (
        {
            type: actions.SIGN_IN_REQUEST,
            userCredentials,
        }
    ),
}

export default actions;