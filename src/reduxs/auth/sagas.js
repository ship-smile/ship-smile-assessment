import { takeEvery, all, delay, race, fork, call, put } from 'redux-saga/effects';
// import { message } from 'antd';
import actions from './actions';
import services from './services';
// import { serialize } from "../../utils/serialize";

function* signinWorker() {
    yield takeEvery(actions.SIGN_IN_REQUEST, function* ({ userCredentials }) {
        try {
            let { res, timeout } = yield race({
                res: call(services.signin, userCredentials),
                timeout: delay(15000),
            });

            if (res.statusCode === 200 && !timeout) {
                yield put({
                    type: actions.SIGN_IN_SUCCESS,
                    accessToken: res.access_token,
                })
                localStorage.setItem("accessToken", res.access_token);
            }

        } catch (error) {
        }
    });
}

export default function* () {
    yield all([
        fork(signinWorker),
    ])
}