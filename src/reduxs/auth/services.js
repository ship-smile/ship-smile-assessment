import SuperFetch from '../../helpers/superFetch';
import { USER_INFO_TYPE } from './actions'

class services {
    signin = async (userCredentials) => {
        return await SuperFetch.post(`signin`, userCredentials).then((response) => {
            return response;
        });
    };
}

export default new services();
