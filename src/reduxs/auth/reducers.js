import actions from "./actions";

const isLoggedIn = localStorage.getItem("isLoggedIn") === "ok" ? true : false;
const initState = {
  isLoggedIn,
  loading: false
};

export default function (state = initState, action) {
  switch (action.type) {
    case actions.SIGN_IN_REQUEST:
      return {
        ...state,
        loading: true
      };

    case actions.SIGN_IN_SUCCESS:
      localStorage.setItem("isLoggedIn", "ok");
      return {
        ...state,
        isLoggedIn: true,
        loadings: false
      };

    case actions.SIGN_IN_ERROR:
      return {
        ...state,
        loading: false
      };

    default:
      return state;
  }
}
