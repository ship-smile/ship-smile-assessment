import configs from '../configs.json';
// import Cookies from 'js-cookie';

class SuperFetch {
  fetchObj;
  methods;
  constructor() {
    this.fetchObj = {};
    this.methods = ['get', 'post', 'put', 'patch', 'delete'];
    this.bindMethod();
  }

  bindMethod() {
    this.methods.forEach((method) => {
      this.fetchObj[method] = this.fetch.bind(this, method);
    });
  }

  async fetch(method, path, bodyObj) {
    try {
      const accessToken = localStorage.getItem("accessToken") || "";
      const response = await fetch(`${configs.API_ENDPOINT}/${path}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${accessToken}`
        },
        method,
        body: method !== 'get' ? JSON.stringify({ ...bodyObj }) : undefined,
      });
      console.log("response.status ", response.status)
      if (response.status === 403) {
        alert("Unauthorized")
        localStorage.removeItem('accessToken');
        localStorage.removeItem('refreshToken');
        localStorage.removeItem('isLoggedIn');
        window.location.reload();
      }
      const json = await response.json();
      return { ...json, statusCode: response.status };
    } catch (error) {
      console.log('error', error);
    }
  }

  get instance() {
    return this.fetchObj;
  }
}

export default new SuperFetch().instance;
