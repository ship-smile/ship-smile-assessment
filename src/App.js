import './App.css';
import * as React from 'react';
import RootRoute from './routes';
import { Provider } from 'react-redux';
import { store } from './reduxs/store';

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <RootRoute />
      </Provider>
    );
  }
}
