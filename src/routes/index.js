import * as React from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter as Router, Routes, Route, useLocation, Navigate, useSearchParams } from 'react-router-dom';
import Layout from '../components/layout';
import { Address } from '../screens';
import { Signin } from '../screens';
export default function RootRoute() {
  return (
    <Router>
      <Routes>
        <Route>
          {/* =========== public route ===========*/}
          <Route
            path="/"
            element={
              <RedirectToApp>
                <Navigate to={'/signin'} />
              </RedirectToApp>
            }
          />
          <Route
            path="/signin"
            element={
              <RedirectToApp>
                <Signin />
              </RedirectToApp>
            }
          />
          {/* =========== private route ===========*/}
          <Route element={<Layout />}>
            <Route
              path="/address"
              element={
                <RequireAuth>
                  <Address />
                </RequireAuth>
              }
            />
          </Route>
        </Route>
        <Route
          path="*"
          element={
            <RedirectToApp>
              <Signin />
            </RedirectToApp>
          }
        />
      </Routes>
    </Router>
  );
}

function RedirectToApp({ children }) {
  let location = useLocation();
  const authReducers = useSelector((state) => state.authReducers);
  console.log("authReducer ", authReducers);
  if (authReducers.isLoggedIn === true) {
    return <Navigate to="/address" state={{ from: location }} replace />;
  }
  return children;
}

function RequireAuth({ children }) {
  let location = useLocation();
  const authReducers = useSelector((state) => state.authReducers);
  if (authReducers.isLoggedIn === false) {
    return <Navigate to="/signin" state={{ from: location }} replace />;
  }
  return children;
}
